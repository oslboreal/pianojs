/* Canvas Setup */
var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");
canvas.width = 700;
canvas.height = 500;
document.body.appendChild(canvas);

/* Music sheet Setup */
let pentagramWidth = 600;
let penta = new MusicalPentagram(pentagramWidth, 60);

/* Action statuses */
let movePlayingLine = false;

function loop() {
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    penta.draw();
    penta.drawPlayingLine();

    if(movePlayingLine)
    {
        penta.moveLine();
        movePlayingLine = false;
    }

    requestAnimationFrame(loop);
} 

loop();

function moveLine()
{
    movePlayingLine = true;
}

let bpm = 200;

setInterval(moveLine, 1000 * 60 / bpm);